const mongoose = require("mongoose");

const connectDb = (url) => {
  return mongoose.connect(url, {
    // disable deprecation warnings
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  });
};

module.exports = connectDb;
